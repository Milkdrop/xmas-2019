Alice and Bob managed to get their hands on quantum technology! They now use it to do evil stuff, like factoring numbers :O

They want to send data between their computers, but the problem is, only Bob has a quantum computer, Alice has a classic one!

They cannot transmit qubits between their computers, so Alice thought it would be smart to use a technique she already knows, but only send instructions to Bob's Quantum computer in order to transmit data.

Can you figure out what Alice told Bob?

Difficulty: Hard
Author: Reda