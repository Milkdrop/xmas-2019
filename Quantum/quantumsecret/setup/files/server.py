import itertools
import numpy as np
import pyquil.api as api
from pyquil.gates import *
from pyquil.quil import Program

FLAG = open('/chall/flag.txt', 'r').read().strip()

print("")
print("You will have to complete the exercise 3 times in a row")
print("")



def qubit_strings(n):
    qubit_strings = []
    for q in itertools.product(['0', '1'], repeat=n):
        qubit_strings.append(''.join(q))
    return qubit_strings

def black_box_map(n, a):
    """
    Black-box map, f(x) = x.a for all vectors x, given a
    """
    qubs = qubit_strings(n)
    # calculate each dot product x.a and store in a dict
    d_blackbox = {}
    for q in qubs:
        dot_prod = 0
        for i, xx in enumerate(q):
            dot_prod += a[i] * int(xx)
        d_blackbox[q] = dot_prod % 2

    return d_blackbox

def qubit_ket(qub_string):
    """
    Form a basis ket out of n-bit string specified by the input 'qub_string', e.g.
    '001' -> |001>
    """
    e0 = np.array([[1], [0]])
    e1 = np.array([[0], [1]])
    d_qubstring = {'0': e0, '1': e1}

    # initialize ket
    ket = d_qubstring[qub_string[0]]
    for i in range(1, len(qub_string)):
        ket = np.kron(ket, d_qubstring[qub_string[i]])
    
    return ket

def projection_op(qub_string):
    """
    Creates a projection operator out of the basis element specified by 'qub_string', e.g.
    '101' -> |101> <101|
    """
    ket = qubit_ket(qub_string)
    bra = np.transpose(ket)  # all entries real, so no complex conjugation necessary
    proj = np.kron(ket, bra)
    return proj

def black_box(n, a):
    """
    Unitary representation of the black-box operator on (n+1)-qubits, given the vector a
    """
    d_bb = black_box_map(n, a)
    # initialize unitary matrix
    N = 2**(n+1)
    unitary_rep = np.zeros(shape=(N, N))
    # populate unitary matrix
    for k, v in d_bb.items():
        unitary_rep += np.kron(projection_op(k), np.eye(2) + v*(-np.eye(2) + np.array([[0, 1], [1, 0]])))
        
    return unitary_rep

def validGate(t):
    if t == "I" or t == "X" or t == "Y" or t == "Z" or t == "H" or t == "-":
        return True
    else:
        return False

correct = 0

while correct < 3:
    print("Intial State |00000>")
    print("Before sending the qubits, you can apply quantum gates to them")
    print("Please choose between the I, X, Y, Z, H gates, or input - to stop applying gates to the certain qubit.")
    p = Program()
    n = 5
    a = np.random.randint(low=0, high=2, size=n)

    #Prepare initial quantum state
    for n_ in range(1, n+2):
        p.inst(I(n_))
    p.inst(X(0))
    p.inst(H(0))
    
    # print("First, you can prepare the state for the control qubit")
    # ccc = False
    # while not ccc:
    #     print("Enter gate for the control Qubit: ")
    #     tm = input()
    #     if tm == "I":
    #         #Identity Gate
    #         p.inst(I(0))
    #     elif tm == "X":
    #         #Flip gate
    #         p.inst(X(0))
    #     elif tm == "Y":
    #         p.inst(Y(0))
    #     elif tm == "Z":
    #         #Phase shift gate
    #         p.inst(Z(0))
    #     elif tm == "H":
    #         #Hardmard gate
    #         p.inst(H(0))
    #     elif tm == "-":
    #         ccc = True
    #     else:
    #         print("Quantum Gate not Found. Exiting...")
    #         exit()

    #Let player setup his state (modify only with Q gates)
    for j in range(1, 6):
        ccc = False
        while not ccc:
            print("Enter gate for Qubit #{}".format(str(j)))
            tm = input()
            if tm == "I":
                #Identity Gate
                p.inst(I(n+1-j))
            elif tm == "X":
                #Flip gate
                p.inst(X(n+1-j))
            elif tm == "Y":
                p.inst(Y(n+1-j))
            elif tm == "Z":
                #Phase shift gate
                p.inst(Z(n+1-j))
            elif tm == "H":
                #Hardmard gate
                p.inst(H(n+1-j))
            elif tm == "-":
                ccc = True
            else:
                print("Quantum Gate not Found. Exiting...")
                exit()

    #RUN Q-Oracle
    p.defgate("U_f", black_box(n, a))
    p.inst(("U_f",) + tuple(range(n+1)[::-1]))

    print("You can now apply other quantum gates to the Qubits before measurement:")
    print("Please choose between the I, X, Y, Z, H gates, or input - to stop applying gates to the certain qubit.")
    for j in range(1, 6):
        ccc = False
        while not ccc:
            print("Enter gate for Qubit #{}".format(str(j)))
            tm = input()
            if tm == "I":
                #Identity Gate
                p.inst(I(n+1-j))
            elif tm == "X":
                #Flip gate
                p.inst(X(n+1-j))
            elif tm == "Y":
                p.inst(Y(n+1-j))
            elif tm == "Z":
                #Phase shift gate
                p.inst(Z(n+1-j))
            elif tm == "H":
                #Hardmard gate
                p.inst(H(n+1-j))
            elif tm == "-":
                ccc = True
            else:
                print("Quantum Gate not Found. Exiting...")
                exit()

    print("")
    print("Measuring qubits...")
    classical_regs = list(range(n))
    for i, n_ in enumerate(list(range(1, n+1))[::-1]):
        p.measure(n_, classical_regs[i])
    
    qvm = api.QVMConnection()
    measure_n_qubits = qvm.run(p, classical_regs)
    measure_n_qubits = [item for sublist in measure_n_qubits for item in sublist]

    print ("Measurement: {}".format(measure_n_qubits))

    print("Now please enter the secret string. example: 01100")
    ss = input()

    corr = ""
    for i in a:
        corr += str(i)

    if ss == corr:
        correct += 1
        print("Good job! {}/3 done!".format(str(correct)))
    else:
        print("You got it wrong!")
        exit()

print("Nice! You got it! FLAG: {}".format(FLAG))