# Use python3 please

import random, string

maxWordLength = 101

FLAG = open("/chall/flag.txt", "r").read()
alphabet = string.ascii_uppercase + string.ascii_lowercase
word = ''.join(random.choice(alphabet) for _ in range(random.randint (90, maxWordLength)))

intro = '''
                                     ,,,,
                                  ,########,
                                 ############
                                |############|
                                 ############
                                  "########"
                                     """"

                     |\\___/|
                     | " " |                    ~ ORAKEL ~
              ,===__/( \\ / )\\__===,        THE LAPLAND  ORACLE
             /     """ (") """     \\
            /           "           \\
            |   \\_____=   =_____/   |
      ,==._/    /\\     /^\\     /\\    \\_.==,
     |   _  __/"  \\   |] [|   /  "\\__  _   |
      555 """      |  |] [|  |      """ 555
"""""""""""""""""""###########""""""""""""""""""""""""""""""""""""""""""""
--- ,#######   ,#############, ,########  ___     _________
 -- #####################################"       _____     _________
    "###" #######################" ____     ___   _____           __
  ---_____  "#############   _           _________     ____
  ______     ##########        ______                _______
         ____"##  "##   _________            ___        ________  _____
    ___       ____    __     _________            _______


'''

description = """> I will give you the True Flag you seek, but for that you must pass my test:
I will think of a word of great length, known only by the gods that roam Lapland.
You must guess which word I am thinking of, but only under a limited number of [1000] tries.
In order to make this possible for you, I will tell you how close you are to my word through a number.
The higher the number, the further you are from my word.
If the number is 0, then you have found it.

Good Luck.
"""

greetings = ["Hello child.", "Welcome,", "I expected you...", "Santa told me about you..."]
results = ["This is your number: ", "I tell you: ", "I must say: ", "Your number: ", "You have: "]

print (intro)
print (random.choice (greetings))
print ("")
print (description)
print ("")

seeds = []
alphabetLength = len (alphabet)

for i in range (maxWordLength):
    seed = []
    seed.append (0)
        
    for i in range (1, alphabetLength):
        seed.append (seed[i - 1] + random.randint (1, 10))
    
    seeds.append (seed)

tries = 0

while True:
    guess = input ("Tell me your guess: ").strip()[:200]
    valid = True
    
    for c in guess:
        if (c not in alphabet):
            valid = False
            break
    
    if (valid == False):
        print ("Words are made of letters.")
        print ("")
        continue
        
    num = 0
    for i in range (len (word)):
        maxwrong = seeds[i][len (alphabet) - 1]
        
        if (i >= len(guess)):
            num += maxwrong
        else:
            charDistance = abs (alphabet.find(word[i]) - alphabet.find(guess[i]))
            num += seeds[i][charDistance]
    
    for i in range (len (word), len (guess)):
        num += maxwrong
    
    print (random.choice (results) + str(num))
    print ("")
    
    if (num == 0):
        print ("Masterfully done. Here is the True Flag: " + FLAG)
        break
    
    tries += 1
    if (tries >= 1000):
        print ("You have used all your tries. I am sorry.")
        break