import numpy as np
import scipy.linalg as la
import random

points = []

with open('coords.txt', 'r') as f:
    points = list(eval(f.read()))
#print points


for i in range(1, len(points), 1):
    ok = 0
    while (ok != 1):
        x_prime = points[i][0] + 1
        y_prime = points[i][1] + 1
        x = points[i - 1][0] + 1
        y = points[i - 1][1] + 1

        a = random.randint(1, 5)
        b = (1.0 * (x_prime - a * x)) / y
        c = b
        d = (1.0 * (y_prime - c * x)) / y

        A = np.array([[a, b], [c, d]])
        eigenValues, eigenVectors = la.eig(A)

        idx = eigenValues.argsort()[::-1]
        eigenValues = eigenValues[idx]
        eigenVectors = eigenVectors[:,idx]

        if (np.linalg.matrix_rank(eigenVectors) == 2):
            ok = 1
            print ("det: {}".format((a * d - b * c)))
            print ("trace: {}".format((a + d)))
            print ("modal: {}".format(eigenVectors))
            print ("eigenvalues: {}".format(eigenValues))
