import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as la
import re
from PIL import Image

img = Image.new ("RGB", [200, 200], "white")
pixels = img.load ()

stuff = []

with open('trans.txt', 'r') as f:
    lines = f.readlines()
    for i in range(0, len(lines), 5):
        det = eval(lines[i].split(':')[1].strip())
        trace = eval(lines[i + 1].split(':')[1].strip())
        line1 = re.sub(' +', ' ', lines[i + 2].split(':')[1].strip().replace('[', '').replace(']', '').strip())
        line2 = re.sub(' +', ' ', lines[i + 3].strip().replace('[', '').replace(']', '').strip())

        #print(line1, line2)

        a = eval(line1.split(' ')[0].strip())
        b = eval(line1.split(' ')[1].strip())
        c = eval(line2.split(' ')[0].strip())
        d = eval(line2.split(' ')[1].strip())
        #print (a, b, c, d)
        modal = np.array([[a, b], [c, d]])
        stuff.append((det, trace, modal))


point = np.array([[1, 0], [10, 0]])
pixels [100, 100] = (255, 255, 255)

img.save("out.png")

exit (0)

for x in stuff:
    D = x[0]
    T = x[1]
    modal = x[2]
    #print (modal)

    lambda1 = (T + (T * T - 4 * D) ** (0.5)) / 2.0
    lambda2 = (T - (T * T - 4 * D) ** (0.5)) / 2.0
 
    eigen1 = np.array([[modal[0][0], 0], [modal[1][0], 0]])

    diag = np.array([[lambda1, 0], [0, lambda2]])
    diag2 = np.array([[lambda2, 0], [0, lambda1]])

    modal_inv = np.linalg.inv(modal)

    transf = np.dot(np.dot(modal, diag), modal_inv)

    if (abs(transf[0][0] * eigen1[0][0] + transf[0][1] * eigen1[1][0] - lambda1 * eigen1[0][0]) > 0.5):
        print ("aici")
        diag = np.array([[lambda2, 0], [0, lambda1]])
        transf = np.dot(np.dot(modal, diag), modal_inv)

    #print (modal)
    #print (modal_inv)
    #print (transf)
    #print (np.dot(np.dot(modal_inv, diag2), modal))
    #print (lambda1, lambda2)
    #print(np.dot(transf, modal))

    point = np.around(np.dot(transf, point))
    
    #print('Points:')
    print ("PNTTTT")
    print (point[0][0], point[1][0])
    #pixels [int (point[0][0]), int (point[1][0])] = (255, 255, 255)
    #plt.scatter(point[0][0], point[1][0], color = 'black')
#plt.show()

img.save ("out.png")
