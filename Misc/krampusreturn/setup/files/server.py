import subprocess, time, random, os
import threading

whitelist = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789()\n"
characters = ["Server", "Elf #" + str (random.randint (0, 5000)), "Icy Dragon", "Golem", "Krampus", "UNKNOWN"]
msgs = [[], ["What is going on down there?", "Yee, what are y'all doing?", "I should go investigate. Later."], ["*puffs smoke*", "*rumble*", "*growls*"], ["*looks towards you*"]]

didnthear = ["He did not hear you.", "You will have to talk louder.", "Try something different.", "He is busy.", "No response.", "..."]
didntlike = ["Krampus did not like that.", "Krampus is not satisfied.", "Krampus is losing his interest. Be Careful.", "Krampus grows angry.", "Senseless words.", "The chamber trembles. Be Careful.", "You hear trembling."]

def printChar (msg, i = 0):
	print ("<{}>: {}".format (characters [i], msg), flush = True)

def die (timeout):
    time.sleep (timeout)
    os._exit (0)

print ("░ ▒ ▓ █", flush = True)
time.sleep (1)
print ("CONNECTION ESTABLISHED - PYTHON 2.")
print ("CONNECTION WILL BE CLOSED AFTER 5 MINUTES. BE WARY.\n")
printChar ("Krampus stares at you. He has seen you before. Play nice.")

thr = threading.Thread (target = die, args = (60 * 5,))
thr.start ()

i = 0
death = 0

while True:
	inp = input ("<You>: ")[:2048] + "\n"

	if (i < 3):
		printChar ("You tried to get Krampus' attention. " + random.choice (didnthear))
	elif (i == 3):
		printChar ("Krampus has heard you. You made him curious. ░ ▒ ▓ █")
	elif (i == 4):
		printChar ("Krampus has now your attention. Be careful!")
	else:
		validInput = True
		for c in inp:
			if c not in whitelist:
				validInput = False

		if (validInput):
			krampus = subprocess.Popen (["python", "/chall/krampus.py"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

			wildcard = 0
			while (krampus.poll () == None):
				if (wildcard == 1):
					inp = input ()[:2048]

				krampus.stdin.write (inp.encode ("ASCII"))
				krampus.stdin.flush ()

				if (wildcard == 1):
					krampus.stdin.close ()
				else:
					wildcard = 1

				time.sleep (1)

			out = krampus.stdout.read ().decode ("ASCII")
			err = krampus.stderr.read ().decode ("ASCII")

			if (out != ""):
				printChar (out[:-1], 4)
								
			if (err != ""):
				printChar (random.choice (didntlike))
				death += 1
		else:
			time.sleep (0.5)
			choice = random.randint (1, 3)
			printChar (random.choice (msgs [choice]), choice)
			death += 1

	i += 1

	if (death == 20):
		printChar ("*Unsheathes Sword*", 3)
		time.sleep (0.5)
		printChar ("That's enough.", 3)
		time.sleep (0.2)
		printChar ("***\x07", 5)
		print ("░ ▒ ▓ █")
		print ("CONNECTION FORCEFULLY TERMINATED.")
		break