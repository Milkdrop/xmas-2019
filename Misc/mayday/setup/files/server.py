from datetime import datetime
from time import sleep

import os
import sys
import time
import threading

#import socket

begin_transmission = '#' * 30 + "   Incoming Emergency Broadcast" + ' ' * 18 + '#' * 70 + '\n'
display_dateandtime = '#' * 30 + "   " + str(datetime.now()) + ' ' * 20 + '#' * 70 + '\n'
end_transmission = '#' * 30 + "   End of Transmission. Connection terminated.   " + '#' * 70 + '\n'
hq_transmission = '#' * 30 + "   CONNECTING TO EMERGENCY RESPONSE TEAM" + ' ' * 9 + '#' * 70 + '\n' 

hq_question1 = "Connection established succesfully. Welcome, user. Do you know who is targeting our agent?\n"

location = "35.70407437075822.139.5577317304603"

signals = ["Mayday", "mayday", "MAYDAY", "MaydaY"]
# Mayday and mayday are number signals
# MAYDAY is a separator signal

class StoppableThread(threading.Thread):
    def __init__(self,  *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

def die (timeout):
    time.sleep (timeout)
    os._exit (0)

def handle_client():
    print(begin_transmission, flush = True)
    sleep(1)
    
    print(display_dateandtime, flush = True)
    sleep(4)
    
    print("\n\n", flush = True)
    
    op = 0
    message = ""
    for ch in location:
        if ch == '.':
            message += signals[2] * 500
        elif ch == '0':
            message += signals[3] * 50 
        else:
            no = int(ch)
            message += signals[op] * no
            op = 1 - op
    
    print(message)
        
    print("\n\n", flush = True)
    sleep(1)

    print(end_transmission, flush = True)
    sleep(1)

    print(hq_transmission, flush = True)
    sleep(1)
    print(hq_question1, flush = True)
    
    tOut = StoppableThread(target = die, args = (5 * 60,))
    tOut.start ()
    
    answer = str(input()).strip().lower()
    answer = answer[:7]
    
    tOut.stop()
    
    if answer == "pigeons":
        print("Those bloody pigeons! We'll get them next time!\n")
        print("Anyway, here is your flag: \n")
        
        f = open("/chall/flag.txt", "r")
        flags = f.readlines()
        print(str(flags[0]), flush = True)
    else:
        print("Nope.\n", flush = True)
    
handle_client()

