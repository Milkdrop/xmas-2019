#include <stdio.h>
#include <string.h>

char flag_matrix[][2] = { "[", ".", "N", "B", "P", "x", "6", "7", "m", "4", "7", "\\", "2", "6", "\\", "a", "7", "g", "\\", "7", "4", "\\", "o", "2", "`", "0", "m", "6", "0", "\\", "`", "k", "0", "`", "h", "2", "m", "5", "~",  };

int main() {
    char license[100];
    printf("Enter your license key: ");
    scanf("%100s", license);

    int ok = 0;
    char tmp[2];
    for (int i = 0; license[i] != '\0'; i++) {
        tmp[0] = license[i] ^ 0x3;
        tmp[1] = '\0';

        ok |= strcmp(tmp, flag_matrix[i]);
    }

    if (ok == 0) {
        printf("License key is correct\n");
    } else {
        printf("License key is incorrect\n");
    }

    return 0;
}
