import random, string

f = open ("malenames.txt", "r").read().split("\n")
males = []
for line in f:
    line = line.split()[0]
    males.append (line)

f = open ("familynames.txt", "r").read().split("\n")
familynames = []
for line in f:
    line = line.split ()
    if (len(line) >= 1):
        line = line[0]
        familynames.append (line)

f = open ("femalenames.txt", "r").read().split("\n")
females = []
for line in f:
    line = line.split()[0]
    females.append (line)

f = open ("addresses.txt", "r").read().split("\n")
addresses = []
for line in f:
    line = line.split()[1:]
    line = ' '.join (line)
    addresses.append (line)

f = open ("diagnosis.txt", "r").read().split("\n")
commondiagnosis = []
for line in f:
    commondiagnosis.append (line)
    
races = ["Caucasian", "Black", "Asian", "Multiracial", "Caucasian", "Caucasian", "Caucasian", "Caucasian"]
months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
bloodtypes = ["0-", "0+", "A-", "A+", "B-", "B+", "AB-", "AB+"]

query = 'INSERT INTO patients (patient_id, name, surname, gender, address, race, birthdate, bloodtype, weight, height, diagnosis) VALUES ("{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", {8}, {9}, "{10}");'

for i in range (3157):
    patient_id = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(16))
    
    surname = random.choice (familynames)
    gender = random.randint (0, 1)
    
    if (gender == 0):
        name = random.choice (females)
        gender = "F"
    elif (gender == 1):
        name = random.choice (males)
        gender = "M"
    
    address = str(random.randint (1, 80000)) + " " + random.choice(addresses)
    race = random.choice (races)
    birthdate = str(random.randint (1, 28)) + " " + random.choice (months) + " " + str(random.randint (1920, 1999))
    bloodtype = random.choice (bloodtypes)
    weight = str(random.randint (70, 120))
    height = str(random.randint (160, 210))
    diagnosis = random.choice (commondiagnosis)
    
    print (query.format(patient_id, name, surname, gender, address, race, birthdate, bloodtype, weight, height, diagnosis))
    