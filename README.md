# X-MAS CTF 2019 Challenge repository

## Repo Main Structure:
Category > Challenge Name

## How/Where to upload a challenge:

- Place any files you want (scripts you used to make the chall, challenge solvers, etc) under `Category/YourChallenge/`

- Place the files needed to run the chall (for example server.py), alongside flag.txt, description.txt and eventually a Dockerfile under `Category/YourChallenge/setup`

- Place STRICTLY the files that need to be uploaded on the CTF platform for ALL the players to see and download under `Category/YourChallenge/public`

## Incomplete/Non-Standardized Challenges (NEED FIX):

- Misc/Noise - Which file should be uploaded for the players? flag.wav or wierd.wav?
    Reda: wierd.wav (its placed inside the files folder) should pe uploaded for the players