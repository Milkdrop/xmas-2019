Please first install the `qemu-user` and `qemu-user-static` packages

Build the docker with: `docker build -t YOUR_CONTAINER_NAME .`

Run the docker in interactive mode with `docker run -it YOUR_CONTAINER_NAME /bin/bash`