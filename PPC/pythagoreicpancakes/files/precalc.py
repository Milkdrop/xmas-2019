import sys
import heapq

h = []
h.append([[5],[4],[3],0])
A=[[1,-2,2],
   [2,-1,2],
   [2,-2,3]]

B=[[1,2,2],
   [2,1,2],
   [2,2,3]]

C=[[-1,2,2],
   [-2,1,2],
   [-2,2,3]]

def transform(x):
    c=[x[2-i] for i in range(3)]
    return c

def Multiply(b,a):
    c=[[0 for i in range(len(b[0]))]for j in range(len(a))]
    for i in range(len(a)):
        for j in range(len(b[0])):
            for k in range(len(b)):
                c[i][j]+=a[i][k]*b[k][j]
    return c

def reorder(x,o):
    if o == 1:
        x[1],x[0]=x[0],x[1]
    return x

def order(x):
    o = 0
    if(x[1]<x[2]):
        x[2],x[1]=x[1],x[2]
        o = 1
    x.append(o)
    return x

f = open(sys.argv[2],"w")

def solve(n):
    global h
    h = []
    h.append([[5],[4],[3],0])
    ans = None

    for i in range(n):
        ans=heapq.heappop(h)
        if i >= n - 10**4 - 1:
            out = transform(ans[:3])
            out = out[0]+out[1]+out[2]
            f.write(','.join([str(a) for a in out])+'\n')
        x=transform(ans[:3])
        o = ans[3]
        x = reorder(x,o)
        xa=order(transform(Multiply(x,A)))
        xb=order(transform(Multiply(x,B)))
        xc=order(transform(Multiply(x,C)))
        heapq.heappush(h, xa)
        heapq.heappush(h, xb)
        heapq.heappush(h, xc)
    return ans


solve(20 * 3 ** int(sys.argv[1]) + 10 ** 4)
