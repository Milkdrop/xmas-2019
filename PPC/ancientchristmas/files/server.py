from math import log
from time import *
import os
import random        
from hashlib import sha256
from binascii import *
from PIL import Image
from io import BytesIO
import base64
from secret import FLAG

SAMPLE_SIZE = 100

def check_intersection(new_coord, coord_list):
        i, j = new_coord

        for coord in coord_list:
                x, y = coord
                if abs(x - i) < 63 or abs(y - j) < 63:
                        return True
        return False

def PoW():
        s = os.urandom(10)
        h = sha256(s).hexdigest()
        inp = input("Provide a hex string X such that sha256(X)[-6:] = {}\n".format(h[-6:]))
        is_hex = 1
        for c in inp:
            if not c in '0123456789abcdef':
                is_hex = 0

        if is_hex and sha256(unhexlify(inp.encode())).hexdigest()[-6:] == h[-6:]:
            print('Good, you can continue!\n')
            return True
        else:
            print('Oops, your string didn\'t respect the criterion.\n')
            return False

if not PoW():
        exit()

error = False

for round_idx in range(1, 11):
        print("Base64 encoded image for challange #{}:".format(round_idx))
        y = 650 * round_idx
        x = 650 * round_idx
        output = BytesIO()

        img = Image.new(mode = "RGB", size = (x, y), color = (255, 255, 255))
        pix=img.load()
        
        symbols_cnt = random.randint(3 * int(log(round_idx + 2) * round_idx), int(3 * log(round_idx + 2) * round_idx))
        symbols_coords = []
        answer = [0 for j in range(6)]
                        
        for symbol in range(symbols_cnt):
                symbol_idx = random.randint(0, 5)
                answer[symbol_idx] += 1
                symbol_val = random.randint(0, SAMPLE_SIZE - 1)
                symbol_img = Image.open("./Symbols/{}/output/{}.bmp".format(str(symbol_idx), str(symbol_val)))

                symbol_coords = (random.randint(0, x - 63), random.randint(0, y - 63))
                is_intersecting = check_intersection(symbol_coords, symbols_coords)

                while is_intersecting:
                        symbol_coords = (random.randint(0, x - 63), random.randint(0, y - 63))
                        is_intersecting = check_intersection(symbol_coords, symbols_coords)

                symbols_coords.append(symbol_coords)
                img.paste(symbol_img, symbol_coords)
                symbol_img.close()
        img.save(output, format = "png")
        output.seek(0)
        print(base64.b64encode(output.read()))  
        img.close()
        start = time()
        inp = input("Give me the number of symbols separated by commas (e.g. 0,1,2,3,4,5)\n")
        end = time()
        if(end - start > 20):
                print("Response took too long. Try moving faster!")
                error = True
                break
        try:
                inp = [int(a) for a in inp.split(',')]
                assert len(inp) == 6
        except:
                print("Invalid input. Aborting!")
                error = True
                break
        if inp != answer:
                print("Wrong answer. Better luck next time!")
                error = True
                break
        print("Well done! Next one.")

if error:
        exit()

print("Congratulations, you are now a symbol recognition master!\nHere's your flag: {}".format(FLAG))
