intro = "Hey there fella! I'm Rudolph's assistant, he has prepared something magic.\nTry to solve this task he left for you."

chall_intro = "Here's challange #{}:\nmessage 1: {} -> ciphertext 1: {}\nmessage 2: [REDACTED] -> ciphertext 2: {}"
chall_request = "Rudolph wants to know what the second message was.\n"

challange_win = "Good, next challange"
challange_loose = "Sadly, Rudolph say you're wrong. Try again!"

invalid_input = "Invalid input. Aborting!"

win = "Well done, you made Rudolph happy.\nHere's your flag: {}"
FLAG = 'X-MAS{5_b17_t00_1nd3p3nden7_f0r_my_t45t3}'
