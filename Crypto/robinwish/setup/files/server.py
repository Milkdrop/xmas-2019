#!/usr/bin/python3

from Crypto.Util.number import long_to_bytes, bytes_to_long

FLAG = "X-MAS{4cTu4lLy_15s_Sp3lLeD_r4b1n_n07_r0b1n_69316497123aaed43fc0}"

letter = FLAG.ljust(250, 'X').encode()

# Rabin params
p = 130961628296256509203688558167635593723553913356760543890382119434581162340507411906149081831530791465121490809254472572128660282537887134954728668333303763155790295871693652007851065436374283916442512241694633008063722724742602750333491782821019252639698534922880498906462617380877186177574727639637556921771
q = 130961628296256509203688558167635593723553913356760543890382119434581162340507411906149081831530791465121490809254472572128660282537887134954728668333303763155790295871693652007851065436374283916442512241694633008063722724742602750333491782821019252639698534922880498906462617380877186177574727639637556922883
e = 2
n = p * q

banner = """  __          ___       __ 
 (_   /\\  |\\ | |  /\\ / (_  
 __) /--\\ | \\| | /--\\  __) 
                           
     _ ___ ___ _  _        
 |  |_  |   | |_ |_)       
 |_ |_  |   | |_ | \\       
                           
  __  _  _      ___  _  _  
 (_  |_ |_) \\  / |  /  |_  
 __) |_ | \\  \\/ _|_ \\_ |_  
                           

Welcome to Santa's letter encryption service!

Enter your letter here and get an encrypted version which only Santa can read.
Note: Hackers beware: are using military-grade encryption!"""

menu = """Menu:
1. Encrypt a letter
2. Encrypt your favourite number
3. Exit
1337. Get Robin's Encrypted Letter (added by elf)"""

enc_letter = pow(bytes_to_long(letter), e, n)

print(banner)

while True:
    print(menu)
    try:
        c = int(input("Choice: "))
    except:
        print("Invalid choice!\n")
        continue

    if c == 1:
        print("For performance reasons, this demo only encrypts one line no longer than 250 characters")
        msg = input("Message: ")
        if len(msg) > 250:
            print("OMG! It's so big!\n")
            continue
        enc = pow(bytes_to_long(msg.encode()), e, n)
        print("Encrypted: {}\n".format(enc))
    elif c == 2:
        num = input("Your favourite number: ")
        try:
            num = int(num)
        except:
            print("That's not a number!\n")
            continue
        if num > n - 100:
            print("Nobody's number is that big! Are you a hacker?\n")
            continue
        enc_num = pow(num, e, n)
        print("You can share your favourite number by sending him the following number: {}".format(enc_num))
    elif c == 3:
        print("Bye!")
        break
    elif c == 1337:
        print("Encrypted letter: {}\n".format(enc_letter))
    else:
        print("Invalid choice!\n")
