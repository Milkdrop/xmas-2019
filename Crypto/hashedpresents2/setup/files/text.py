intro = "Hello, if you shall pass my challenge {} times you will prove worthy of this flag.\n"

losing_outro = "Not, quite there, how 'bout you try a little harder next time?\n"
winning_outro = "You do A LOT of HARDCORE MATH in your spare time don't you?\nAnyway congrats, here's your flag: {}\n"

chall_intro = "Starting chall #{}. Here's the string: {} and the hash: {}.\n" 
chall_ok = "Nice one, keep going!\n"
chall_wrong = "Sorry, that's not the correct answer :(\n"

FLAG = 'X-MAS{D01ng_X0r_1nst34d_0f_4dd1t10n_15_h4rd3r}'
