#!/usr/bin/python3
import subprocess
from Crypto.Cipher import AES
from Crypto.Util.number import long_to_bytes
from key import key

def unpad(ct):
    return ct[:-(ct[-1])]

def decrypt(ct):
    return AES.new(key, AES.MODE_CBC, ct[:16]).decrypt(ct[16:])

enc_cmd = long_to_bytes(int(input('santa@northpole ~$ '), 16))
cmd = unpad(decrypt(enc_cmd))

print(subprocess.check_output(cmd.split(b' ')).decode())
