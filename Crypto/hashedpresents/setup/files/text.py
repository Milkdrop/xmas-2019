intro = "Hello, if you shall pass my challenge {} times you will prove worthy of this flag.\n"

losing_outro = "Not, quite there, how 'bout you try a little harder next time?\n"
winning_outro = "You do quite a lot of mental gymnastics in your spare time don't you?\nAnyway congrats, here's your flag: {}\n"

chall_intro = "Starting chall #{}. Here's the string: {} and the hash: {}.\n" 
chall_ok = "Nice one, keep going!\n"
chall_wrong = "Sorry, that's not the correct answer :(\n"

FLAG = 'X-MAS{w31l_17_s33ms_y0u_r34l1y_kn0w_s0m3_1in3ar_4lg3br4}'
