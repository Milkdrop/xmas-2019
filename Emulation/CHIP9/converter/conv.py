from PIL import Image
import sys

img = Image.open ("img.png")
rgb_im = img.convert('RGB')

for i in range (64):
    for k in range (0, 128, 8):
        strn = ""
        for bit in range (0, 8):
            #print (rgb_im.getpixel ((k + bit, i)))
            #print (rgb_im.getpixel ((k + bit, i)))
            if (rgb_im.getpixel ((k + bit, i)) == (255, 255, 255)):
                strn += "1"
            else:
                strn += "0"
        #sys.stdout.write (strn)
        sys.stdout.write (hex (int (strn, 2))[2:] + " ")
    sys.stdout.write ("\n")
    
sys.stdout.flush ()
