#include <SDL2/SDL.h>
#include <stdint.h>
#include <stdio.h>

typedef int8_t i8;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

inline u8 GetBit (u16 byte, u16 position) {
	return (byte & (1 << position)) != 0;
}

u8 RAM [0x10000];

int main (int argc, char* argv[]) {
	if (argc < 2) {
		printf ("Please give me the ROM: %s rom.bin\n", argv[0]);
		return 1;
	}

	SDL_Init (SDL_INIT_EVERYTHING);

	printf ("Opening the Boot ROM: ./bios...\n");
	FILE* fin = fopen ("bios", "rb");
	u8 bios[0x10000];
	u16 buffSize = fread (bios, 1, 0x325, fin);
	fclose (fin);

	if (buffSize == 0) {
		printf ("Couldn't Open Boot ROM File: ./bios\n");
		return 1;
	}

	printf ("Opening ROM: %s...\n", argv[1]);
	fin = fopen (argv[1], "rb");
	u8 rom[0x10000];
	u16 romSize = fread (rom, 1, 0x1000, fin);
	fclose (fin);

	printf ("%d", fin);
	if (romSize == 0) {
		printf ("Couldn't Open ROM File: %s\n", argv[1]);
		return 1;
	}

	memcpy (RAM + 0x325, bios, buffSize);
	memcpy (RAM + 0x597, rom, romSize);

	SDL_Event ev;
	u8 quit = 0;

	while (quit == 0) {
		while (SDL_PollEvent (&ev)) {
			if (ev.type == SDL_QUIT)
				quit = 1;
		}
	}

	return 0;
}