#include "CPU.h"

CPU::CPU (Display* newDisp) {
	B = ((u8*) &BC) + 1;
	C = ((u8*) &BC);
	D = ((u8*) &DE) + 1;
	E = ((u8*) &DE);
	H = ((u8*) &HL) + 1;
	L = ((u8*) &HL);

	A = &realA;

	halt = 0;
	PC = 0x0;

	disp = newDisp;
}

void CPU::LoadInMemory (u16 addr, u8* buffer, u16 size) {
	memcpy (RAM + addr, buffer, size);
}

inline u8 GetBit (u16 byte, u16 position) {
	return (byte & (1 << position)) != 0;
}

inline u16 CPU::GetWord (u16 addr) {
	return RAM [addr + 1] * 0x100 + RAM [addr];
}

inline void CPU::SetWord (u16 addr, u16 value) {
	RAM[addr] = value & 0xFF;
	RAM[addr + 1] = value >> 8;
}

inline void CPU::Push16 (u16 value) {
	SetWord (SP, value);
	SP -= 2;
}

inline void CPU::Push8 (u8 value) {
	RAM [SP--] = value;
	SP--;
}

inline u16 CPU::Pop16 () {
	SP += 2;
	u16 value = GetWord (SP);
	return value;
}

inline u8 CPU::Pop8 () {
	SP++;
	return RAM [++SP];
}

inline void CPU::SetFlags (u8 opA, u8 opB) {
	u16 sum = opA + opB;

	flag_Z = (sum & 255) == 0;
	flag_N = (sum & 128) != 0;
	flag_H = ((sum ^ opA ^ opB) & 16) != 0;
	flag_C = ((sum ^ opA ^ opB) & 256) != 0;

	// printf ("Flags %d %d = %d / %d - %d %d %d %d\n", opA, opB, smallResult, bigResult, flag_Z, flag_N, flag_H, flag_C);
}

inline void CPU::SetFlagsSigned (i8 opA, i8 opB) {
	u16 sum = opA + opB;

	flag_Z = (opA == opB);
	flag_N = (opA < opB);
	flag_H = ((sum ^ opA ^ opB) & 16) != 0;
	flag_C = ((sum ^ opA ^ opB) & 256) != 0;
}

void CPU::PrintInfo () {
	printf ("BC: 0x%04x\n", BC);
	printf ("B: 0x%02x\n", *B);
	printf ("C: 0x%02x\n", *C);
	printf ("DE: 0x%04x\n", DE);
	printf ("D: 0x%02x\n", *D);
	printf ("E: 0x%02x\n", *E);
	printf ("HL: 0x%04x\n", HL);
	printf ("H: 0x%02x\n", *H);
	printf ("L: 0x%02x\n", *L);

	printf ("M: 0x%02x\n", *M);
	printf ("A: 0x%02x\n", *A);

	printf ("PC: 0x%04x\n", PC);
	printf ("SP: 0x%04x\n", SP);

	printf ("Flags: ZNHC ");
	printf (flag_Z==1?"1":"0");
	printf (flag_N==1?"1":"0");
	printf (flag_H==1?"1":"0");
	printf (flag_C==1?"1":"0");
	printf ("\n");
}

void CPU::Step () {
	if (halt)
		return;

	u8 opcode = RAM [PC++];
	u16 xxyy = (RAM[PC + 1] << 8) + RAM [PC];

	M = &RAM [HL];

	switch (opcode) {

		// MEMORY MOVEMENT

		case 0x20: *B = RAM [PC]; PC++; break; // LDI
		case 0x30: *C = RAM [PC]; PC++; break; // LDI
		case 0x40: *D = RAM [PC]; PC++; break; // LDI
		case 0x50: *E = RAM [PC]; PC++; break; // LDI
		case 0x60: *H = RAM [PC]; PC++; break; // LDI
		case 0x70: *L = RAM [PC]; PC++; break; // LDI
		case 0x80: *M = RAM [PC]; PC++; break; // LDI
		case 0x90: *A = RAM [PC]; PC++; break; // LDI

		case 0x21: BC = xxyy; PC += 2; break; // LDX
		case 0x31: DE = xxyy; PC += 2; break; // LDX
		case 0x41: HL = xxyy; PC += 2; break; // LDX
		case 0x22: SP = xxyy; PC += 2; break; // LDX

		case 0x81: Push8 (*B); break; // PUSH
		case 0x91: Push8 (*C); break; // PUSH
		case 0xA1: Push8 (*D); break; // PUSH
		case 0xB1: Push8 (*E); break; // PUSH
		case 0xC1: Push8 (*H); break; // PUSH
		case 0xD1: Push8 (*L); break; // PUSH
		case 0xC0: Push8 (*M); break; // PUSH
		case 0xD0: Push8 (*A); break; // PUSH

		case 0x82: *B = Pop8 (); break; // POP
		case 0x92: *C = Pop8 (); break; // POP
		case 0xA2: *D = Pop8 (); break; // POP
		case 0xB2: *E = Pop8 (); break; // POP
		case 0xC2: *H = Pop8 (); break; // POP
		case 0xD2: *L = Pop8 (); break; // POP
		case 0xC3: *M = Pop8 (); break; // POP
		case 0xD3: *A = Pop8 (); break; // POP

		case 0x51: Push16 (BC); break; // PUSH
		case 0x61: Push16 (DE); break; // PUSH
		case 0x71: Push16 (HL); break; // PUSH

		case 0x52: BC = Pop16 (); break; // POP
		case 0x62: DE = Pop16 (); break; // POP
		case 0x72: HL = Pop16 (); break; // POP

		case 0x09: *B = *B; break; // MOV
		case 0x19: *B = *C; break; // MOV
		case 0x29: *B = *D; break; // MOV
		case 0x39: *B = *E; break; // MOV
		case 0x49: *B = *H; break; // MOV
		case 0x59: *B = *L; break; // MOV
		case 0x69: *B = *M; break; // MOV
		case 0x79: *B = *A; break; // MOV

		case 0x89: *C = *B; break; // MOV
		case 0x99: *C = *C; break; // MOV
		case 0xA9: *C = *D; break; // MOV
		case 0xB9: *C = *E; break; // MOV
		case 0xC9: *C = *H; break; // MOV
		case 0xD9: *C = *L; break; // MOV
		case 0xE9: *C = *M; break; // MOV
		case 0xF9: *C = *A; break; // MOV

		case 0x0A: *D = *B; break; // MOV
		case 0x1A: *D = *C; break; // MOV
		case 0x2A: *D = *D; break; // MOV
		case 0x3A: *D = *E; break; // MOV
		case 0x4A: *D = *H; break; // MOV
		case 0x5A: *D = *L; break; // MOV
		case 0x6A: *D = *M; break; // MOV
		case 0x7A: *D = *A; break; // MOV

		case 0x8A: *E = *B; break; // MOV
		case 0x9A: *E = *C; break; // MOV
		case 0xAA: *E = *D; break; // MOV
		case 0xBA: *E = *E; break; // MOV
		case 0xCA: *E = *H; break; // MOV
		case 0xDA: *E = *L; break; // MOV
		case 0xEA: *E = *M; break; // MOV
		case 0xFA: *E = *A; break; // MOV

		case 0x0B: *H = *B; break; // MOV
		case 0x1B: *H = *C; break; // MOV
		case 0x2B: *H = *D; break; // MOV
		case 0x3B: *H = *E; break; // MOV
		case 0x4B: *H = *H; break; // MOV
		case 0x5B: *H = *L; break; // MOV
		case 0x6B: *H = *M; break; // MOV
		case 0x7B: *H = *A; break; // MOV

		case 0x8B: *L = *B; break; // MOV
		case 0x9B: *L = *C; break; // MOV
		case 0xAB: *L = *D; break; // MOV
		case 0xBB: *L = *E; break; // MOV
		case 0xCB: *L = *H; break; // MOV
		case 0xDB: *L = *L; break; // MOV
		case 0xEB: *L = *M; break; // MOV
		case 0xFB: *L = *A; break; // MOV

		case 0x0C: *M = *B; break; // MOV
		case 0x1C: *M = *C; break; // MOV
		case 0x2C: *M = *D; break; // MOV
		case 0x3C: *M = *E; break; // MOV
		case 0x4C: *M = *H; break; // MOV
		case 0x5C: *M = *L; break; // MOV
		case 0x6C: halt = 1; break; // HCF
		case 0x7C: *M = *A; break; // MOV

		case 0x8C: *A = *B; break; // MOV
		case 0x9C: *A = *C; break; // MOV
		case 0xAC: *A = *D; break; // MOV
		case 0xBC: *A = *E; break; // MOV
		case 0xCC: *A = *H; break; // MOV
		case 0xDC: *A = *L; break; // MOV
		case 0xEC: *A = *M; break; // MOV
		case 0xFC: *A = *A; break; // MOV

		case 0xED: HL = BC; break; // MOV
		case 0xFD: HL = DE; break; // MOV

		// ARITHMETIC

		case 0x08: flag_Z = 0; flag_N = 0; flag_H = 0; flag_C = 0; break; // CLR FLAGS
		case 0x18: flag_Z = 1; break; // SETFLAG
		case 0x28: flag_Z = 0; break; // SETFLAG
		case 0x38: flag_N = 1; break; // SETFLAG
		case 0x48: flag_N = 0; break; // SETFLAG
		case 0x58: flag_H = 1; break; // SETFLAG
		case 0x68: flag_H = 0; break; // SETFLAG
		case 0x78: flag_C = 1; break; // SETFLAG
		case 0x88: flag_C = 0; break; // SETFLAG

		case 0x04: SetFlags (*B, *A); *B += *A; break; // ADD
		case 0x14: SetFlags (*C, *A); *C += *A; break; // ADD
		case 0x24: SetFlags (*D, *A); *D += *A; break; // ADD
		case 0x34: SetFlags (*E, *A); *E += *A; break; // ADD
		case 0x44: SetFlags (*H, *A); *H += *A; break; // ADD
		case 0x54: SetFlags (*L, *A); *L += *A; break; // ADD
		case 0x64: SetFlags (*M, *A); *M += *A; break; // ADD
		case 0x74: SetFlags (*A, *A); *A += *A; break; // ADD
		case 0xA7: SetFlags (*A, RAM[PC]); *A += RAM[PC++]; break; // ADDI

		case 0x83: SetFlags (BC, *A); BC += *A; break; // ADD
		case 0x93: SetFlags (DE, *A); DE += *A; break; // ADD
		case 0xA3: SetFlags (HL, *A); HL += *A; break; // ADD

		case 0x84: SetFlags (*B, -*A); *B -= *A; break; // SUB
		case 0x94: SetFlags (*C, -*A); *C -= *A; break; // SUB
		case 0xA4: SetFlags (*D, -*A); *D -= *A; break; // SUB
		case 0xB4: SetFlags (*E, -*A); *E -= *A; break; // SUB
		case 0xC4: SetFlags (*H, -*A); *H -= *A; break; // SUB
		case 0xD4: SetFlags (*L, -*A); *L -= *A; break; // SUB
		case 0xE4: SetFlags (*M, -*A); *M -= *A; break; // SUB
		case 0xF4: SetFlags (*A, -*A); *A -= *A; break; // SUB
		case 0xB7: SetFlags (*A, -RAM[PC]); *A -= RAM[PC++]; break; // SUBI

		case 0x03: SetFlags (*B, 1); (*B)++; break; // INC
		case 0x13: SetFlags (*C, 1); (*C)++; break; // INC
		case 0x23: SetFlags (*D, 1); (*D)++; break; // INC
		case 0x33: SetFlags (*E, 1); (*E)++; break; // INC
		case 0x43: SetFlags (*H, 1); (*H)++; break; // INC
		case 0x53: SetFlags (*L, 1); (*L)++; break; // INC
		case 0x63: SetFlags (*M, 1); (*M)++; break; // INC
		case 0x73: SetFlags (*A, 1); (*A)++; break; // INC

		case 0xA8: BC++; break; // INX
		case 0xB8: DE++; break; // INX
		case 0xC8: HL++; break; // INX

		case 0x07: SetFlags (*B, -1); (*B)--; break; // DEC
		case 0x17: SetFlags (*C, -1); (*C)--; break; // DEC
		case 0x27: SetFlags (*D, -1); (*D)--; break; // DEC
		case 0x37: SetFlags (*E, -1); (*E)--; break; // DEC
		case 0x47: SetFlags (*H, -1); (*H)--; break; // DEC
		case 0x57: SetFlags (*L, -1); (*L)--; break; // DEC
		case 0x67: SetFlags (*M, -1); (*M)--; break; // DEC
		case 0x77: SetFlags (*A, -1); (*A)--; break; // DEC

		// LOGICAL OPERATIONS

		case 0x05: *B &= *A; SetFlags (*B, 0); break; // AND
		case 0x15: *C &= *A; SetFlags (*C, 0); break; // AND
		case 0x25: *D &= *A; SetFlags (*D, 0); break; // AND
		case 0x35: *E &= *A; SetFlags (*E, 0); break; // AND
		case 0x45: *H &= *A; SetFlags (*H, 0); break; // AND
		case 0x55: *L &= *A; SetFlags (*L, 0); break; // AND
		case 0x65: *M &= *A; SetFlags (*M, 0); break; // AND
		case 0x75: *A &= *A; SetFlags (*A, 0); break; // AND
		case 0xC7: *A &= RAM[PC++]; SetFlags (*A, 0); break; // ANDI

		case 0x85: *B |= *A; SetFlags (*B, 0); break; // OR
		case 0x95: *C |= *A; SetFlags (*C, 0); break; // OR
		case 0xA5: *D |= *A; SetFlags (*D, 0); break; // OR
		case 0xB5: *E |= *A; SetFlags (*E, 0); break; // OR
		case 0xC5: *H |= *A; SetFlags (*H, 0); break; // OR
		case 0xD5: *L |= *A; SetFlags (*L, 0); break; // OR
		case 0xE5: *M |= *A; SetFlags (*M, 0); break; // OR
		case 0xF5: *A |= *A; SetFlags (*A, 0); break; // OR
		case 0xD7: *A |= RAM[PC++]; SetFlags (*A, 0); break; // ORI

		case 0x06: *B ^= *A; SetFlags (*B, 0); break; // XOR
		case 0x16: *C ^= *A; SetFlags (*C, 0); break; // XOR
		case 0x26: *D ^= *A; SetFlags (*D, 0); break; // XOR
		case 0x36: *E ^= *A; SetFlags (*E, 0); break; // XOR
		case 0x46: *H ^= *A; SetFlags (*H, 0); break; // XOR
		case 0x56: *L ^= *A; SetFlags (*L, 0); break; // XOR
		case 0x66: *M ^= *A; SetFlags (*M, 0); break; // XOR
		case 0x76: *A ^= *A; SetFlags (*A, 0); break; // XOR
		case 0xE7: *A ^= RAM[PC++]; SetFlags (*A, 0); break; // XORI

		case 0x86: SetFlags (*B, -*A); break; // CMP
		case 0x96: SetFlags (*C, -*A); break; // CMP
		case 0xA6: SetFlags (*D, -*A); break; // CMP
		case 0xB6: SetFlags (*E, -*A); break; // CMP
		case 0xC6: SetFlags (*H, -*A); break; // CMP
		case 0xD6: SetFlags (*L, -*A); break; // CMP
		case 0xE6: SetFlags (*M, -*A); break; // CMP
		case 0xF6: SetFlags (*A, -*A); break; // CMP
		case 0xF7: SetFlags (*A, -RAM[PC++]); break; // CMPI

		case 0x0D: SetFlagsSigned (*B, *A); break; // CMPs
		case 0x1D: SetFlagsSigned (*C, *A); break; // CMPs
		case 0x2D: SetFlagsSigned (*D, *A); break; // CMPs
		case 0x3D: SetFlagsSigned (*E, *A); break; // CMPs
		case 0x4D: SetFlagsSigned (*H, *A); break; // CMPs
		case 0x5D: SetFlagsSigned (*L, *A); break; // CMPs
		case 0x6D: SetFlagsSigned (*M, *A); break; // CMPs
		case 0x7D: SetFlagsSigned (*A, *A); break; // CMPs
		
		// I/O

		case 0xE0: scanf ("%c", A); break; // SIN
		case 0xE1: printf ("%c", *A); break; // SOUT
		case 0xF0: disp->Render (); disp->Clear (); break; // CLRSCR
		case 0xF1: disp->Draw ((i8) *C, (i8) *B, *A); break; // DRAW

		// BRANCHING

		case 0x0F: PC = xxyy; break; // JMP
		case 0x1F: if (flag_Z) PC = xxyy; else PC += 2; break; // JMPZ
		case 0x2F: if (!flag_Z) PC = xxyy; else PC += 2; break; // JMPNZ
		case 0x3F: if (flag_N) PC = xxyy; else PC += 2; break; // JMPN
		case 0x4F: if (!flag_N) PC = xxyy; else PC += 2; break; // JMPNN
		case 0x5F: if (flag_H) PC = xxyy; else PC += 2; break; // JMPH
		case 0x6F: if (!flag_H) PC = xxyy; else PC += 2; break; // JMPNH
		case 0x7F: if (flag_C) PC = xxyy; else PC += 2; break; // JMPC
		case 0x8F: if (!flag_C) PC = xxyy; else PC += 2; break; // JMPNC

		case 0x9F: PC += (i8) RAM[PC]; PC++; break; // JMP NEAR
		case 0xAF: if (flag_Z) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR
		case 0xBF: if (!flag_Z) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR
		case 0xCF: if (flag_N) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR
		case 0xDF: if (!flag_N) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR
		case 0xEF: if (flag_H) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR
		case 0xFF: if (!flag_H) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR
		case 0xEE: if (flag_C) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR
		case 0xFE: if (!flag_C) {PC += (i8) RAM[PC];} PC++; break; // JMP NEAR

		case 0x1E: Push16 (PC + 2); PC = xxyy; break; // CALL
		case 0x0E: PC = Pop16 (); break; // RET

		// MISC

		case 0x00: break; // NOP
		case 0x11: PrintInfo (); break; // Undocumented DEBUG

		default:
			printf ("Undefined Opcode: 0x%02x at 0x%04x\n", opcode, PC - 1);
	}
}