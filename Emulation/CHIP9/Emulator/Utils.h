#ifndef UTILS_H
#define UTILS_H

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdint.h>
#include <string>
#include <time.h>
#include <chrono>
using namespace std::chrono;

typedef int8_t i8;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

u8 GetBit (u16 byte, u16 position);
u64 GetCurrentTime ();

#endif