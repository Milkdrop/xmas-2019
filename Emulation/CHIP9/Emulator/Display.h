#ifndef DISPLAY_H
#define DISPLAY_H
#include "Utils.h"

class Display {

	public:
		Display (const char* title, u16 _width, u16 _height, u8 _pixelSize);
		void Render ();
		void Draw (i8 posX, i8 posY, u8 byte);
		void Clear ();
		void SetPixel (u8 posX, u8 posY, u8 set);

	private:
		u8 pixelSize;
		u16 width, height;
		u8 pixels [128 * 64];
		SDL_Window* mainWindow;
		SDL_Renderer* mainRenderer;
		SDL_Texture* mainTexture;

		u8 BGColor = 0x00;
		u8 Color = 0xFF;
};

#endif