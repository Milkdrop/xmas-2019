#include "Display.h"
#include "CPU.h"
#include <unistd.h>

inline u64 GetCurrentTime () {
	auto now = high_resolution_clock::now ();
	return duration_cast <milliseconds> (now.time_since_epoch ()).count ();
}

int main (int argc, char* argv[]) {
	setbuf (stdout, NULL);
	
	if (argc < 2) {
		printf ("Please give me the ROM: %s rom.bin\n", argv[0]);
		return 1;
	}

	SDL_Init (SDL_INIT_EVERYTHING);
	Display disp ("CHIP9", 128, 64, 2);
	CPU cpu (&disp);

	printf ("Opening the Boot ROM: ./bios...\n");
	FILE* fin = fopen ("bios", "rb");
	u8 buff[0x10000];
	u16 ROMSize = fread (buff, 1, 0x325, fin);
	fclose (fin);

	if (ROMSize == 0) {
		printf ("Couldn't Open Boot ROM File: ./bios\n");
		return 1;
	}

	cpu.LoadInMemory (0x0, buff, ROMSize);

	printf ("Opening ROM: %s...\n", argv[1]);
	fin = fopen (argv[1], "rb");
	ROMSize = fread (buff, 1, 0x10000, fin);
	fclose (fin);

	if (ROMSize == 0) {
		printf ("Couldn't Open ROM File: %s\n", argv[1]);
		return 1;
	}

	//cpu.LoadInMemory (0x597, buff, ROMSize);
	cpu.LoadInMemory (0, buff, ROMSize);

	SDL_Event ev;
	u8 quit = 0;

	u8 eventDelay = 0;
	u64 drawDelayms = 10;
	u64 inputDelayms = 1;
	u64 lastDraw = 0;
	u64 lastInput = 0;

	u8 pressedShift = 0;
	u8 pressedEnter = 0;

	const uint8_t *Keyboard = SDL_GetKeyboardState (NULL);

	while (quit == 0) {
		/*u64 currentTime = GetCurrentTime ();

		if (currentTime - lastDraw >= drawDelayms) {
			lastDraw = currentTime;
			//disp.Render ();
		}

		eventDelay++;

		if ((eventDelay & 0b111111) == 0 && false) {
			//lastInput = currentTime;
			while (SDL_PollEvent (&ev)) {
				if (ev.type == SDL_QUIT)
					quit = 1;
			}

			if (!Keyboard [SDL_SCANCODE_UP])
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b01111111;
			else
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 7);

			if (!Keyboard [SDL_SCANCODE_LEFT])
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b10111111;
			else
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 6);

			if (!Keyboard [SDL_SCANCODE_DOWN])
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b11011111;
			else
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 5);

			if (!Keyboard [SDL_SCANCODE_RIGHT])
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b11101111;
			else
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 4);

			if (!Keyboard [SDL_SCANCODE_Z])
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b11110111;
			else
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 3);

			if (!Keyboard [SDL_SCANCODE_X])
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b11111011;
			else
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 2);

			if (!Keyboard [SDL_SCANCODE_LSHIFT]) {
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b11111101;
				pressedShift = 0;
			}
			else if (pressedShift == 0) {
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 1);
				pressedShift = 1;
			}

			if (!Keyboard [SDL_SCANCODE_RETURN]) {
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) & 0b11111110;
				pressedEnter = 0;
			}
			else if (pressedEnter == 0) {
				cpu.RAM[0xF000] = (cpu.RAM[0xF000]) | (1 << 0);
				pressedEnter = 1;
			}

		}*/

		cpu.Step ();
	}

	return 0;
}
