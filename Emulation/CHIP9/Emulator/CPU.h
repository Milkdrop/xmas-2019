#ifndef CPU_H
#define CPU_H
#include "Utils.h"
#include "Display.h"

class CPU {
	public:
		CPU (Display* newDisp);
		void LoadInMemory (u16 addr, u8* buffer, u16 size);
		void Step ();
		u8 RAM[0x10000];

	private:
		u16 GetWord (u16 addr);
		void SetWord (u16 addr, u16 value);
		void SetFlags (u8 opA, u8 opB);
		void SetFlagsSigned (i8 opA, i8 opB);
		void PrintInfo ();

		void Push16 (u16 value);
		void Push8 (u8 value);
		u16 Pop16 ();
		u8 Pop8 ();

		u8 halt;
		u16 BC, DE, HL;
		u8 realA;
		u8 *B, *C, *D, *E, *H, *L, *M, *A;

		u16 PC, SP;

		u8 flag_Z;
		u8 flag_N;
		u8 flag_H;
		u8 flag_C;

		Display* disp;
		u8 render = 0;
		int debug = 0;
};

#endif