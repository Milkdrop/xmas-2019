8 bit little endian CPU

8bit registers
B C D E H L M A
M is always the byte pointed by HL = (HL)

16bit register pairs:
BC DE HL
16bit registers
PC SP

F = 8bit Flags register
0000ZNHC

BIOS:
Small ROM that initializes the machine

BC = 0
DE = 0
HL = 0
A = 0

PC = 0x0100
SP = 0xFFFF
VRAM -> 0xFF

display htsp logo

Memory Layout:
0x0000 - 0x0100 Interrupts

0xFF00 - 0xFFFF Stack
