f = open ("rom.asm", "r").read ().split ("\n")
o = open ("rom.asm.byte", "w")

cmds = {
	"LDI" : ("x0", 2),
	"LDX" : ("x1", 2),
	"SERIAL IN": ("E0"),
	"SERIAL OUT": ("E1"),
	"CLRSCR": ("F0"),
}

for line in f:
	line = line.split (" ")