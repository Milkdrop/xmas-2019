import sys

ROM = open("rom", "rb").read ()

PC = 0x100
A = 0

RAM = [0] * 0x1000
Locked = [False] * 0x1000

for i in range (len(ROM)):
	RAM [0x100 + i] = ROM[i]

while (True):
	instr = RAM[PC] * 0x100 + RAM[PC + 1]
	firstHalfByte = (RAM[PC] & 0xF0) >> 4
	threeLastBytes = (RAM[PC] & 0x0F) * 0x100 + RAM[PC + 1]
	valid = False
	
	if (RAM[PC] == 0x00):
		A += RAM[PC + 1]
		valid = True
	elif (RAM[PC] == 0x01):
		A = RAM[PC + 1]
		valid = True
	elif (RAM[PC] == 0x02):
		A ^= RAM[PC + 1]
		valid = True
	elif (RAM[PC] == 0x03):
		A |= RAM[PC + 1]
		valid = True
	elif (RAM[PC] == 0x04):
		A &= RAM[PC + 1]
		valid = True
	elif (RAM[PC] == 0x13 and RAM[PC + 1] == 0x37):
		sys.stdout.write (chr(A))
		sys.stdout.flush ()
		valid = True
	elif (RAM[PC] == 0x60):
		if (RAM[PC + 1] == A):
			A = 0
		elif (RAM[PC + 1] > A):
			A = 1
		elif (RAM[PC + 1] < A):
			A = 255
		valid = True
	elif (RAM[PC] == 0xBE and RAM[PC + 1] == 0xEF):
		A = 0x42
		PC = 0x100 - 0x2
		valid = True
	elif (RAM[PC] == 0xEE and RAM[PC + 1] == 0xEE):
		valid = True
	if (firstHalfByte == 0x08):
		A = RAM[threeLastBytes]
		valid = True
	elif (firstHalfByte == 0x0D):
		if (Locked [threeLastBytes] == False):
			RAM[threeLastBytes] ^= A
		valid = True
	elif (firstHalfByte == 0x0F):
		if (Locked [threeLastBytes] == False):
			RAM[threeLastBytes] = A
		valid = True
	elif (firstHalfByte == 0x02):
		PC = threeLastBytes - 0x2
		valid = True
	elif (firstHalfByte == 0x03):
		if (A == 0):
			PC = threeLastBytes - 0x2
		valid = True
	elif (firstHalfByte == 0x04):
		if (A == 1):
			PC = threeLastBytes - 0x2
		valid = True
	elif (firstHalfByte == 0x05):
		if (A == 255):
			PC = threeLastBytes - 0x2
		valid = True
	elif (firstHalfByte == 0x07):
		if (RAM[threeLastBytes] == A):
			A = 0
		elif (RAM[threeLastBytes] > A):
			A = 1
		elif (RAM[threeLastBytes] < A):
			A = 255
		valid = True
	elif (firstHalfByte == 0x09):
		Locked [threeLastBytes] = True
		valid = True
	elif (firstHalfByte == 0x0A):
		Locked [threeLastBytes] = False
		valid = True
	elif (firstHalfByte == 0x0C):
		if (Locked [threeLastBytes] == False):
			RAM[threeLastBytes] ^= 0x42
		valid = True
	
	if (valid == False):
		A -= 1 # Illegal instruction
		
	if (A < 0):
		A += 256
	elif (A >= 256):
		A -= 256
	
	#print (str(hex(PC)) + ": " + str(hex(A)))
	
	PC += 0x2