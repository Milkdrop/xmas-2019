#include <stdio.h>
#include <string.h>
#include <seccomp.h>
 
void load_syscall_filter() {
    scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
 
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigreturn), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(open), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
 
    seccomp_load(ctx);
}
 
int main() {
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    load_syscall_filter();
 
    char buf[256];
    puts("Hello, who are you?");
    fgets(buf, 256, stdin);
    printf("Oh, greetings ");
    printf(buf);
    printf("! Long time no see...\n");
 
    printf("Please, write your letter to Santa\n");
    while (!strstr(buf, "end of letter")) {
        fgets(buf, 256, stdin);
        printf("Okok, I am taking notes, so you said: ");
        printf(buf);
    }
    printf("Bye, bye, see you next year!");
 
    return 0;
}
