#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <map>
#include <string>
#include <thread>
#include <chrono>
#include <iostream>
#include <sstream>
#include <cstdio>

namespace py = pybind11;
using namespace std;

class Test {
    public:
        static int add(int a, int b) {
            return a + b;
        }
};

#define py_acquire(x) {\
    py::gil_scoped_acquire acq; \
    x; \
    py::gil_scoped_release rel;}

class Task {
    private:
        int n;
        int *arr;
    public:
        Task(vector<long long int> v) {
            if (v.size() * 4 >= 0x100) return ;
            n = v.size();
            arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = v[i];
            }
            
        }

        void remake(int k) {
            //if (n != 0) return ;
            if (k * 4 >= 0x100) return ;
            n = k;
            arr = new int[k];

            //cout<<"new: " << (void*)arr << endl;
        }

        void update(vector<long long int> v) {
            if (n == 0) return ;
            for (int i = 0; i < n; i++) {
                arr[i] = v[i];
            }
        }

        int sum(int from, int to) {
            if (from < 0 or from >= n or to < 0 or to >= n) return -123;

            int s = 0;
            for (int i = from; i <= to; i++) {
                s = Test::add(s, arr[i]);
            }

            return s;
        }

        void release() {
            //cout << "deleted: " << (void*)arr << endl;
            n = 0;
            delete[] arr;
        }

        void rest(int seconds) {
            this_thread::sleep_for(chrono::seconds(seconds));
        }

        ~Task() {
            release();
            arr = nullptr;
        }
};

Task* make_task(vector<long long int> v) {
    return new Task(v);
}

enum action_types {
    UPDATE,
    REMAKE,
    ATTACH,
    SUM_I_J,
    REST,
    RELEASE,
    END,
};

struct action_args {
    action_types type;
    py::dict args;

    action_args(action_types m_type, py::dict m_args) {
        type = m_type;
        args = m_args;
    }
};

class Elf {
    private:
        Task *t;
        py::dict return_dict; 
        vector<action_args> actions;
   
        void rest(py::dict d) {
            int seconds = d["seconds"].cast<int>();
            t->rest(seconds);
        }

        void sum_i_j(py::dict d) {
            int i = d["i"].cast<int>();
            int j = d["j"].cast<int>();

            int s = t->sum(i, j);
           
            py_acquire(return_dict["sum"] = py::cast(s);)
        }

        void attach(py::dict d) {
            if (t != nullptr) {
                return ;
            }


            py_acquire(t = d["task"].cast<Task*>());
        }

        void release(py::dict d) {
            t->release();
        }

        void remake(py::dict d) {
            int n = d["n"].cast<int>();
            t->remake(n);
        }

        void update(py::dict d) {
            py_acquire(
                py::list l = d["arr"].cast<py::list>();
                vector<long long int> v;
                for (auto e: l) {
                    v.push_back(py::cast<long long int>(e));
                }
                t->update(v);
            );
        }

        void handler() {
            for (auto act: actions) {
                switch (act.type) {
                    case REST:
                        rest(act.args);
                        break;
                    case ATTACH:
                        attach(act.args);
                        break;
                    case SUM_I_J:
                        sum_i_j(act.args);
                        break;
                    case RELEASE:
                        release(act.args);
                        break;
                    case UPDATE:
                        update(act.args);
                        break;
                    case REMAKE:
                        remake(act.args);
                        break;
                    case END:
                        if (t != nullptr) {
                            delete t;
                        }
                    default:
                        break;
                }
            }
        }

    public:
        Elf(vector<action_args> m_actions) {
            t = nullptr;
            actions = m_actions;
            if (actions[actions.size() - 1].type != END) {
                actions.push_back({END, py::dict()});
            }

            py_acquire(
                // passing another argument creates a crash in Python
                // TODO: investigate this, maybe bug in pybind
                thread th(&Elf::handler, this);
                th.detach()
                )
        }

        py::dict get_return_dict() {
            return return_dict;
        }
};

PYBIND11_MODULE(elves, m) {
    m.doc() = "Elves, a multithreaded library"; // optional module docstring 
    py::class_<Test>(m, "Test")
        .def(py::init<>())
        .def("add", &Test::add);

    py::enum_<action_types>(m, "ActionType", py::arithmetic(), "")
        .value("RELEASE", RELEASE, "")
        .value("REST", REST, "")
        .value("ATTACH", ATTACH, "")
        .value("SUM_I_J", SUM_I_J, "")
        .value("UPDATE", UPDATE, "")
        .value("REMAKE", REMAKE, "")
        .export_values();

    py::class_<action_args>(m, "ActionArg")
        .def(py::init<action_types, py::dict>());

    py::class_<Elf>(m, "Elf")
        .def(py::init<vector<action_args>>())
        .def("ret", &Elf::get_return_dict, py::return_value_policy::reference);

    py::class_<Task>(m, "Task");

    m.def("make_task", &make_task, py::return_value_policy::reference);
}

