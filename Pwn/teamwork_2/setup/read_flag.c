#include <stdio.h>

int main() {
	FILE *f = fopen("/home/ctf/flag.txt", "r");
	char buf[256];
	fscanf(f, "%256s", buf);
	printf("Flag: %s\n", buf);
	fclose(f);

	return 0;
}
