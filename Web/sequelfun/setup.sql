CREATE DATABASE IF NOT EXISTS ctf CHARACTER SET utf8 COLLATE utf8_general_ci;
SET GLOBAL query_cache_size = 1000000;
GRANT SELECT ON ctf.* TO 'HTsP'@'%' IDENTIFIED BY 'HTsPAccessKey';

USE ctf;

CREATE TABLE users (
	uid INT UNSIGNED,
	user VARCHAR (255),
	pass VARCHAR (255)
);

INSERT INTO users (uid, user, pass) VALUES (0, "admin", "sJDOSCSn09d1d0wc1nnosad013c89");
INSERT INTO users (uid, user, pass) VALUES (1, "elf", "elf");