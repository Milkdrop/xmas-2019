<?php

session_start();
if(!isset($_SESSION['logged']))
{
	header('location: /');
}

if(isset($_GET['ls']))
{
	echo "Current directory: .backup <br>";
	echo shell_exec("ls /.backup");
	exit();
}

if(isset($_GET['cat']))
{
	$fn = $_GET['cat'];
	if($fn != "firmware.bin" && $fn != "firmware_handler.php" && $fn != "acc.txt"){
		echo "File not Found";
		exit();
	}
	else
	{
		echo shell_exec("cat /.backup/$fn");
		exit();
	}
}

?>

This page is only available to authenticated users. If you are not authenticated, you should be redirected shortly... <br>

<script>
	setTimeout(() => {
		window.location = "/";
	}, 3000);
</script>

Documentation: <br>
?ls - lists files and directories <br>
?cat=&ltfile name&gt - reads file <br>