<?php
session_start();
if(!isset($_SESSION["code"]) || $_SESSION["code"] != "25548857362" || !isset($_SESSION["logged"]) || !$_SESSION['logged'])
{
	header("location: /");
	exit();
}

?>

<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-size: 100%; background-repeat: no-repeat;" background="bg.jpg">
<div style="position: absolute; height: 90%; width: 90%; color: #468da3">
<span style="position: absolute; font-family: 'Raleway', sans-serif; font-size: 2.7vw; right: 10%; top: 24%">Firmware Corrupted. <br> Upload new firmware!</span>
<form id="firmform" method="post" action="/firmware_handler.php" enctype="multipart/form-data">
  <div class="form-group" style="color: black; position: absolute; font-size: 1vw; right: 11%; top:45%">
    <label for="exampleFormControlFile1">Upload firmware</label>
    <input type="file" name="firmware" class="form-control-file" id="exampleFormControlFile1">
  </div>
</form>
<button style="position: absolute; font-size: 1vw; right: 23.5%; top: 58%" onclick="upfirmware()" type="button" class="btn btn-dark">Upload</button>
</div>
<button style="position: absolute; font-size: 1vw; right: 31%; top:60%" onclick="logout()" type="button" class="btn btn-success">Log Out</button>
</body>
<script src="js/bootstrap.min.js"></script>
<script src="js/firm.js"></script>
</html>