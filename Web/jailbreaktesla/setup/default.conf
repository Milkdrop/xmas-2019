limit_req_zone $binary_remote_addr zone=throttled:1m rate=5r/s;

server {
	listen 80;
	listen [::]:80;

	root /var/www/html;
	index index.php index.html;

	server_name ctf;
	server_tokens off;

	keepalive_requests 10;
	keepalive_timeout 10s;

	limit_req zone=throttled burst=20 nodelay;

	gzip on;
	gzip_proxied any;
	gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript audio/mpeg video/mp4;
	gzip_vary on;
	gzip_disable "MSIE [1-6]\.(?!.*SV1)";

	# deny access to . files, for security
	location ~ /\. {
		log_not_found off; 
		deny all;
	}

	location ~* \.(jpg|jpeg|png|gif|ico|css|js|mp3|mp4)$ {
		expires 365d;
	}

	location / {
		try_files $uri $uri/ $uri.html @extensionless-php;
	}

	location ~ \.php$ {
		try_files $uri =404;
		include fastcgi_params;
	    fastcgi_buffers 8 16k;
		fastcgi_buffer_size 32k;
		fastcgi_connect_timeout 60;
	 	fastcgi_send_timeout 300;
		fastcgi_read_timeout 300;
	    fastcgi_pass unix:/var/run/php/php7.3-fpm.sock; # Change this to your current php fpm .sock file
	    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	    fastcgi_param SCRIPT_NAME $fastcgi_script_name;
	}

    location @extensionless-php {
		rewrite ^(.*)$ $1.php last;
	}

	if ($http_user_agent ~* (^w3af.sourceforge.net|dirbuster|nikto|SF|sqlmap|fimap|nessus|whatweb|Openvas|jbrofuzz|libwhisker|webshag) ) {
		return 403;
	}
	
	add_header X-XSS-Protection "1; mode=block";
}
