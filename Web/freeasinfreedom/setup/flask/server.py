from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from flask import Flask, request
from pyvirtualdisplay import Display
import threading, time

app = Flask (__name__)
display = Display (visible=0, size= (320, 240))
display.start ()

queue = []
freetab = [0 for i in range (4)]

@app.route('/',methods = ['POST'])
def route():
	global queue
	try:
		suggestion = request.form ['s']
		queue.append (suggestion)
		return "OK"
	except:
		return "Error"

@app.route('/HTsPfun_queuelen',methods = ['GET'])
def queuelen():
	global queue
	return str (len (queue))

def opentab (i, suggestion):
	global driver
	global allTabs
	global freetab

	freetab [i] = 0
	driver.switch_to.window (allTabs [i])
	driver.get ("http://127.0.0.1/?suggestion=" + suggestion)

	freetab [i] = 1

def driverloop ():
	global queue
	global allTabs
	global freetab

	for i in range (len (allTabs)):
		freetab [i] = 1

	while True:
		try:
			if (len (queue) != 0):
				for i in range (4):
					if (freetab [i] == 1):
						suggestion = queue[0]
						queue = queue[1:]
						thr = threading.Thread (target = opentab, args = (i, suggestion))
						thr.daemon = True
						thr.start ()
			else:
				time.sleep (0.1)
		except:
			pass

options = Options ()
options.add_argument ("--headless")
options.add_argument ("--no-sandbox")
driver = webdriver.Chrome (options = options)

driver.set_window_size (320, 240)
driver.set_page_load_timeout (5)
driver.get ("http://127.0.0.1/")
driver.add_cookie({'name': 'PHPSESSID', 'value': 'idu109ndd9pdsniap0sdnc12cRR1R'})

for i in range (3):
	driver.execute_script ('window.open ("http://127.0.0.1", "_blank");')

allTabs = driver.window_handles
thr = threading.Thread (target = driverloop)
thr.daemon = True
thr.start ()

if __name__ == '__main__':
   app.run (host = '0.0.0.0', port = 2000, debug=False, threaded=True)
