<?php
session_start ();
include_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';
$securimage = new Securimage ();

if ($securimage->check ($_POST['captcha_code']) == false && false) {
	echo "Captcha failed. Please take your time.";
} else {
	if (isset ($_POST['handle']) && isset ($_POST['suggestion'])) {
		$_SESSION['handle'] = $_POST['handle'].trim ();

		$sugg = $_POST['suggestion'].trim ();
		$_SESSION['suggestion'] = $sugg;
		$sugg = urlencode ($sugg);

		if (strlen ($sugg) >= 7900) {
			die ("Your suggestion is too large to be read.");
		}

		$url = 'http://127.0.0.1:2000/';
		$data = array ('s' => $sugg);

		$options = array (
			'http' => array (
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query ($data)
			)
		);

		$context  = stream_context_create ($options);
		$result = file_get_contents ($url, false, $context);

		if (strcmp ($result, "OK") === 0)
			echo "Dr. Stallman will look over your suggestion.";
		else
			echo "An error occured. Maybe Dr. Stallman is busy?<br><br>You can contact Milkdrop.";
	}
}
?>
