CREATE DATABASE IF NOT EXISTS ctf CHARACTER SET utf8 COLLATE utf8_general_ci;
SET GLOBAL query_cache_size = 1000000;
GRANT SELECT ON ctf.* TO 'HTsP'@'%' IDENTIFIED BY 'HTsPAccessKey';

USE ctf;

CREATE TABLE users (
	id INT UNSIGNED,
	name VARCHAR (255),
	description VARCHAR (255)
);

CREATE TABLE flag (
	whatsthis VARCHAR (255)
);

INSERT INTO users (id, name, description) VALUES (1, "Geronimo", "People say he owns a Cadillac ...");
INSERT INTO flag (whatsthis) VALUES ("X-MAS{What?__But_1_Th0ught_Comments_dont_3x3cvt3:(}");