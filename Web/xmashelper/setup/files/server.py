import os, subprocess
import discord, platform, asyncio
import requests, time

client = discord.Client()
token = "NjMxNzg4MTc1MTY1MjI3MDMy.XZ7-Pw.rA8SZM4CSU9nnqpFV19e3tb-tD4"

@client.event
async def on_ready ():
    print ('All good! Name: ' + client.user.name)
    await client.change_presence (activity = discord.Game (name = '!help to get help'))

@client.event
async def on_message (message):
    if (message.author == client.user):
        return

    if (message.content[0] != '!'):
        return

    printer = ""

    if (message.content == "!help"):
        printer = "- **!help** - Show this message\n"
        printer += "- **!flag** - Check the flag to see if it's OK.\n"
        printer += "- **!del** - Delete all challenges."

    elif (message.channel.guild.id == 519974854485737483): # X-MAS Server
        printer = "Unauthorized."

    elif (message.content == "!del"):
        printer = "What do you think you're doing?"

    elif (message.content == "!flag"):
        ok = False
        for role in message.author.roles:
            if (role.name == "Organizer"):
                ok = True

        if (ok):
            printer = "The flag is: **{}**".format ("X-MAS{FREE_FLAGS_FOR_EVERY0NE}")
        else:
            printer = "Unauthorized."
    await message.channel.send (printer)

client.run (token)
client.close ()