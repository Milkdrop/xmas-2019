<?php
include ("db.php");
session_start();

$conn = new mysqli ($servername, $username, $password, $dbname);
if ($conn->connect_error)
	die ("Connection failed: " . $conn->connect_error);

$currentTime = time ();
$lastInsert = file_get_contents (".lastInsert");

if ($lastInsert == "")
	$lastInsert = 0;

if (isset ($_POST['name']) && isset ($_POST['idea'])) {
	if ($currentTime - intval ($lastInsert) >= 1) {
		$q = $conn->prepare ("INSERT INTO ideas (ip, name, idea) VALUES (?, ?, ?)");

		$ip = ip2long ($_SERVER['REMOTE_ADDR']);
		$name = substr (trim ($_POST['name']), 0, 31);
		$idea = substr (trim ($_POST['idea']), 0, 255);

		$q->bind_param ("iss", $ip, $name, $idea);

		if (strlen ($name) === 0)
			$name = "Anonymous";

		if (strlen ($idea) === 0)
			$idea = "I don't have anything to say.";

		if (strpos ($name, 'X-MAS{') !== false || strpos ($idea, 'X-MAS{') !== false) {
			$name = "Anon E. Moose";
			$idea = "I wanted to post the flag.";
		}

		$q->execute ();
		$q->close ();

		$ideaCount = $conn->query ("SELECT LAST_INSERT_ID()")->fetch_array ()[0];
		echo '<div style="text-align: center; margin-top: 5px">The idea you\'ve uploaded is number #' . $ideaCount . ' in our records.</div>';
		file_put_contents (".lastInsert", $currentTime);
	} else {
		echo '<marquee class="error" scrollamount="10">Sorry, but someone else is trying to write his ideas right now. Try again later!</marquee>';
	}
}

$sql = "SELECT * FROM ideas ORDER BY points DESC LIMIT 25";
$result = $conn->query ($sql);
$conn->close ();
?>

<head>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<div class="container">
		<div id="status" class="status invisible"></div>

		<img src="/img/idea.gif">
		<h1 class="head"> Idea Voting System </h1>
		<img src="/img/idea.gif">

		<h2><i>Submit your idea:</i></h2>
		<form style="padding-left: 35px" action="/" name="newIdea" method="post">
			<textarea maxlength="31" style="margin-right: 45px; margin-bottom: 5px; height: 25px" placeholder="Name" cols="30" rows ="1" name="name"></textarea><br>
			<textarea maxlength="255" placeholder="I think that we should do X, because Y ..." cols="30" rows ="5" name="idea"></textarea>
			<input class="submitButton" type="submit" value="">
		</form>

		<?php
			$empty = true;
			$minVotes = 250;

			if ($_SESSION ['votes'] >= $minVotes) {
				echo 'Your computer is quite snappy! In order to congratulate your voting effort, here is our reward:<br>';
				echo '<b>X-MAS{NASA_aint_got_n0thin_on_m3}</b><br><br>';
			}

			while ($row = $result->fetch_assoc ()) {
				$id = $row ['id'];

				echo '<div class="idea ' . (($row ['points'] >= $minVotes)?'special':'') . '">';
				echo '<b class="name">' . htmlspecialchars ($row ['name']) . '</b> / ' . $row ['points'] . ' Points';
				echo '<div class="vote">
						<div class="agree" onclick="vote('. $id . ', 1)">(I agree)</div> / 
						<div class="disagree" onclick="vote(' . $id . ', 0)">(I disagree)</div>
					</div>';
				echo '<div class="ideaText">' . htmlspecialchars ($row ['idea']) . "</div></div>";

				$empty = false;
			}

			if ($empty)
				echo 'No ideas yet.';
		?>

		<footer>
			As seen on Good Morning America
		</footer>

		<script src="/md5.js"></script>
		<script src="/index.js"></script>
	</div>
</body>