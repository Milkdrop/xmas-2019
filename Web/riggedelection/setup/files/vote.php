<?php
include ("db.php");
session_start();

$workLength = 6;

if (!isset ($_SESSION ['work'])) {
	$work = substr (str_shuffle (str_repeat ($x = '0123456789abcdef', ceil ($workLength / strlen ($x)))), 1, $workLength);
	$_SESSION ['work'] = $work;
}

if (isset ($_GET['g']))
	echo $_SESSION ['work'];
else if (isset ($_GET['id']) && isset ($_GET['h'])) {
	$hashThis = trim ($_GET['h']);

	if (preg_match ('/^[a-z0-9]+$/i', $hashThis)) {
		$salt = 'watch__bisqwit__';
		$md5 = md5 ($salt . $hashThis);
		echo "Your md5 is: " . $md5 . "<br>";
		echo "Work required is: " . $_SESSION ['work'] . "<br>";

		if (strcmp (substr ($md5, 0, $workLength), $_SESSION['work']) === 0) {
			$conn = new mysqli ($servername, $username, $password, $dbname);
			if (isset ($_GET['u']))
				$q = $conn->prepare ("UPDATE ideas SET points = points + 1 WHERE id=?");
			else
				$q = $conn->prepare ("UPDATE ideas SET points = points - 1 WHERE id=?"); // Just let them have negative points

			$id = intval ($_GET['id']);
			$q->bind_param ("i", $id);
			$q->execute ();
			$q->close ();
			$conn->close ();
			echo "Work confirmed. Your vote has been registered.";

			if (!isset ($_SESSION ['votes']))
				$_SESSION ['votes'] = 1;
			else 
				$_SESSION ['votes'] += 1;

		} else {
			echo "Work unconfirmed. Your work has changed.";
		}

		unset ($_SESSION ['work']);
	} else {
		echo "String must be alphanumeric";
	}
}
?>