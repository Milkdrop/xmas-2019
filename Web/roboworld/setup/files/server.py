from flask import Flask, render_template, request, session, redirect, send_file
import os
import requests
from captcha import verifyCaptchaValue

port = 2000

app = Flask(__name__, static_url_path="/static")
 
@app.route('/')
def index():
    return render_template("index.html")

@app.route('/login', methods=['POST'])
def login():
    username = request.form.get('user')
    password = request.form.get('pass')
    captchaToken = request.form.get('captcha_verification_value')

    #DEBUGGING: key which allows all requests 
    #privKey = "8EE86735658A9CE426EAF4E26BB0450E"
    privKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" #redacted
    r = requests.get('http://127.0.0.1:{}/captchaVerify?captchaUserValue={}&privateKey={}'.format(str(port), captchaToken, privKey))
    #backdoored ;)))
    if username == "backd00r" and password == "catsrcool" and r.content == b'allow':
        session['logged'] = True
        print("{} LOGGED IN".format(request.remote_addr))
        return redirect('/dashboard_jidcc88574c')
    else:
        return "login failed"


@app.route('/captchaVerify')
def captchaVerify():
    #only 127.0.0.1 has access
    if request.remote_addr != "127.0.0.1":
        return "Access denied"

    token = request.args.get('captchaUserValue')
    privKey = request.args.get('privateKey')

    if(verifyCaptchaValue(token, privKey)):
        return str("allow")
    else:
        return str("deny")


#LEAK STOPS HERE
app.secret_key = "asdhuauhdhu43492nfsnmcmnscna"

@app.route("/dashboard_jidcc88574c")
def dashboard():
    if "logged" in session and session['logged'] == True:
        return render_template("dashboard.html")
    else:
        return "access denied"

@app.route("/wtf.mp4")
def wtfmp4():
    if "logged" not in session or session["logged"] != True:
        return "Not Found", 404
    return send_file('wtf.mp4')

if __name__ == '__main__':
      app.run(host='0.0.0.0', port=port)